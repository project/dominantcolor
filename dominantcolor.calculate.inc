<?php

/**
 * @file
 * Dominant color admin actions.
 */

use ColorThief\ColorThief;

require_once 'src/color.php';

/**
 * Calculate Dominant color for the entity with field.
 */
function dominantcolor_calculate_sefi($setting, $entity, $entity_type, $image_field_name) {
  list($entity_id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

  if (empty($entity->{$image_field_name})) return FALSE;

  $rgbs = array();
  $voc = taxonomy_vocabulary_machine_name_load(variable_get('dominantcolor_voc'));
  $terms = entity_load('taxonomy_term', FALSE, array('vid' => $voc->vid));
  $field_name = variable_get('dominantcolor_hex_field');
  $skip_tid = variable_get('dominantcolor_term_undefined', -1);
  $count = variable_get('dominantcolor_count', 1);
  $delta = 0;
  foreach ($terms as $term) {
    if ($skip_tid == $term->tid) {
      continue;
    }

    $items = field_get_items('taxonomy_term', $term, $field_name);
    if (!empty($items)) {
      foreach ($items as $item) {
        $rgbs[$term->tid. '|' . $delta] = _dominantcolor_calculate_hex2rgb($item['value']);
        $delta++;
      }
    }
  }
  if (empty($rgbs)) {
    return FALSE;
  }

  $term_field_name = $setting['dominantcolor_term_field'];

  $image_field_values = $entity->{$image_field_name};

  $do_save = FALSE;

  foreach ($image_field_values as $langcode => $image_field_values_lang) {
    foreach ($image_field_values_lang as $field_value) {
      if (!empty($field_value['fid'])) {
        $delta = 0;
        $result = dominantcolor_calculate($field_value['fid'], $rgbs, $count);
        if (!empty($result)) {
          foreach ($result as $data) {
            $tid_delta = explode('|', $data);
            $tid = $tid_delta[0];
            $do_save = TRUE;
            $entity->{$term_field_name}[$langcode][$delta]['tid'] = $tid;
            $delta++;
          }
        }
        else {
          if (variable_get('dominantcolor_term_undefined', -1) > 0) {
            $do_save = TRUE;
            $entity->{$term_field_name}[$langcode][$delta]['tid'] = variable_get('dominantcolor_term_undefined', -1);
          }
        }
      }
    }
  }

  if ($do_save) {
    field_attach_presave($entity_type, $entity);
    field_attach_update($entity_type, $entity);
    //entity_get_controller($entity_type)->resetCache(array($entity_id));
  }

}

/**
 * Calculate dominant color, then find closed RGB
 */
function dominantcolor_calculate($fid, $rgbs = array(), $count = 1) {
  if (empty($rgbs)) return FALSE;

  $file = file_load($fid);
  if (!$file) return FALSE;

//  $dominantColor = ColorThief::getColor( drupal_realpath($file->uri));
  $palette_count = $count < 2 ? 2 : $count;
  $palette = ColorThief::getPalette(drupal_realpath($file->uri), 10, 10);

  if (!empty($palette)) {
    $result = array();

    $debug = array();
    foreach ($palette as $dominantColor) {
      $curdebug = '';

      $color = new Color();
      $color = $color->fromRgbInt($dominantColor[0], $dominantColor[1], $dominantColor[2]);

      $curdebug = 'Calculated dominant color: #' . $color->toHex() . ' [' . implode(':', $dominantColor) . ']';

      $cur_palette = array();
      foreach ($rgbs as $key => $rgb) {
        $palette_color = new Color();
        $cur_palette[$key] = $palette_color->fromRgbInt($rgb[0], $rgb[1], $rgb[2]);
      }

      $matchIndex = $color->getClosestMatch($cur_palette);
      if ($matchIndex) {
        $matchColor = $cur_palette[$matchIndex];
        $tid_delta = explode('|',$matchIndex);
        $result[$tid_delta[0]] = $matchIndex;
        $curdebug  .= '<br>Found closest color from defined tax.terms: ' . $matchColor->toHex() . ', index: ' . $matchIndex . '<br>';
      }

      if (variable_get('dominantcolor_debug')) {
        $debug[] = $curdebug ;
      }
    }

    if (variable_get('dominantcolor_debug')) {
      drupal_set_message(implode("<br>", $debug));
    }

    $result = array_slice($result, 0, $count, TRUE);

    return $result;
  }

  return FALSE;
}

/**
 * HEX to RGB
 */
function _dominantcolor_calculate_hex2rgb($hex) {
  $hex = str_replace("#", "", $hex);

  if(strlen($hex) == 3) {
    $r = hexdec(substr($hex,0,1).substr($hex,0,1));
    $g = hexdec(substr($hex,1,1).substr($hex,1,1));
    $b = hexdec(substr($hex,2,1).substr($hex,2,1));
  }
  else {
    $r = hexdec(substr($hex,0,2));
    $g = hexdec(substr($hex,2,2));
    $b = hexdec(substr($hex,4,2));
  }

  $rgb = array($r, $g, $b);
  return $rgb;
}

/**
 * Compare distance RGB colors (absolute)
 */
function _dominantcolor_absoluteColorDistance($colorA, $colorB) {
  return abs($colorA[0] - $colorB[0]) + abs($colorA[1] - $colorB[1]) + abs($colorA[2] - $colorB[2]);
}

/**
 * Compare distance RGB colors (luminosity)
 */
function _dominantcolor_luminanceDistance($colorA, $colorB) {
  $luminance_f = function ($red, $green, $blue) {
    // source: https://en.wikipedia.org/wiki/Relative_luminance
    $luminance = (int) (0.2126*$red + 0.7152*$green + 0.0722*$blue);
    return $luminance;
  };

  return abs(
    $luminance_f($colorA[0], $colorA[1], $colorA[2]) -
    $luminance_f($colorB[0], $colorB[1], $colorB[2])
  );
}
