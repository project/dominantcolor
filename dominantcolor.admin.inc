<?php

/**
 * @file
 * Dominant color admin actions.
 */


/**
 * Admin form.
 */
function dominantcolor_admin($form, &$form_state) {
  $form = array();

  $vocabulary = taxonomy_get_vocabularies();
  $opts = array();
  foreach ($vocabulary as $voc) {
    $opts[$voc->machine_name] = $voc->name;
  }
  $form['dominantcolor_voc'] = array(
    '#type' => 'select',
    '#title' => t('Taxonomy vocabulary with HEX codes'),
    '#options' => $opts,
    '#required' => TRUE,
    '#default_value' => variable_get('dominantcolor_voc'),
  );

  if (variable_get('dominantcolor_voc')) {
    $fields_info = field_info_instances('taxonomy_term', variable_get('dominantcolor_voc'));

    $opts = array();
    foreach ($fields_info as $field_name => $info) {
      $field_info = field_info_field($field_name);
      if ($field_info['type'] == 'text') {
        $opts[$field_name] = $info['label'];
      }
    }
    $form['dominantcolor_hex_field'] = array(
      '#type' => 'select',
      '#title' => t('The field with HEX codes in term'),
      '#options' => $opts,
      '#default_value' => variable_get('dominantcolor_hex_field'),
      '#required' => TRUE,
      '#description' => 'HEX codes stored in this field in associated taxonomy term.'
    );

    $form['dominantcolor_count'] = array(
      '#type' => 'textfield',
      '#title' => t('Count of dominant colors'),
      '#default_value' => variable_get('dominantcolor_count', 1),
      '#attributes' => array(
        ' type' => 'number',
        ' min' => 1,
        ' max' => 10,
      ),
      '#description' => 'How many dominant colors should be calculated. If you will set more than 1, above field should accept multiple values.',
    );

    $voc = taxonomy_vocabulary_machine_name_load(variable_get('dominantcolor_voc'));
    $terms = entity_load('taxonomy_term', FALSE, array('vid' => $voc->vid));
    $opts = array();
    foreach ($terms as $term) {
      $opts[$term->tid] = $term->name;
    }
    $form['dominantcolor_term_undefined'] = array(
      '#type' => 'select',
      '#title' => t('Undefined term'),
      '#options' => $opts,
      '#default_value' => variable_get('dominantcolor_term_undefined', -1),
      '#description' => 'Select undefined term, if dominant color willn\'t calculated.'
    );

  }

  $form['dominantcolor_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug mode'),
    '#default_value' => variable_get('dominantcolor_debug'),
  );

  return system_settings_form($form);
}

/**
 * Implements hook_form_FORM_ID_alter for image type field.
 */
function dominantcolor_admin_field_ui_field_edit_alter(&$form, &$form_state, $form_id) {
  if ($form['#field']['type'] == 'image') {
    form_load_include($form_state, 'inc', 'dominantcolor', 'dominantcolor.admin');

    $dominant_settings = !empty($form['#instance']['dominantcolor']) ? $form['#instance']['dominantcolor'] : $form['#instance']['settings'];
    $form['instance']['dominantcolor'] = array(
      '#type' => 'fieldset',
      '#title' => 'Dominant color',
      '#collapsible' => TRUE,
      '#collapsed' => !$dominant_settings['dominantcolor_enabled'],
    );
    $form['instance']['dominantcolor']['dominantcolor_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Calculate a dominant color of the image'),
      '#default_value' => $dominant_settings['dominantcolor_enabled'],
    );

    $fields_info = field_info_instances($form['#instance']['entity_type'], $form['#instance']['bundle']);
    $opts = array(0 => t('- Undefined -'));
    foreach ($fields_info as $field_name => $info) {
      $field_info = field_info_field($field_name);
      if ($field_info['type'] == 'taxonomy_term_reference') {
        $opts[$field_name] = $info['label'];
      }
    }
    $form['instance']['dominantcolor']['dominantcolor_term_field'] = array(
      '#type' => 'select',
      '#title' => t('Field for storing calculated dominant color'),
      '#options' => $opts,
      '#default_value' => $dominant_settings['dominantcolor_term_field'],
      '#description' => 'The taxonomy term field should exists in field list of this bundle.'
    );
    $form['instance']['dominantcolor']['dominantcolor_term_field_hide'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide dominant color term field from the entity insert/update form'),
      '#description' => t('<i>TODO: Not yet supported</i>'),
      '#default_value' => $dominant_settings['dominantcolor_term_field_hide'],
    );
  }

  $form['#validate'][] = 'dominantcolor_admin_field_ui_field_edit_validate';
}

/**
 * Validate field edit form.
 */
function dominantcolor_admin_field_ui_field_edit_validate($form, &$form_state) {
  if (!empty($form_state['values']['instance']['dominantcolor'])) {
    $dominant = $form_state['values']['instance']['dominantcolor'];
    if ($dominant['dominantcolor_enabled']) {
      if (empty($dominant['dominantcolor_term_field'])) {
        form_set_error('instance][dominantcolor][dominantcolor_term_field', t('You should select taxonomy term field.'));
      }
    }
  }
}
