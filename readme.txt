Dominant Color: Calculate a dominant color of the image.

TODO Better documention

INSTALL:
- Install https://www.drupal.org/project/registry_autoload module.
- Install this module as usual.

CONFIGURE:
1. Create taxonomy term with HEX-color field:
  http://test3.nikiit.ru/admin/structure/taxonomy/color
  Click Add/Edit, and you can fill here the name of the color and it's HEX code.

2. Then you should go to settings of the module:
   http://test3.nikiit.ru/admin/config/media/dominantcolor
   Select here appropriate taxonomy term vocabulary and required field and Undefined term (this is optional).

3. Then go to fields of any node type editing:
   Here settings for image field instance:
   http://test3.nikiit.ru/admin/structure/types/manage/article/fields/field_image
   Here you see "Dominant color" fieldset. If enabled - it will calculate Dominant color, but also you should set "Field for storing calculated dominant color" - of course, you should before create term field associated with vocabulary mention above.

4. So after enabling, on Adding or editing this node dominant color starting work, and save calculated value to "Field for storing calculated dominant color".
   You can see here: http://test3.nikiit.ru/node/5
   Also, you can test self by adding new article content: http://test3.nikiit.ru/node/add/article
